const Discord = require('discord.js');
const config = require("./config.json");
const client = new Discord.Client();
var checkInterval = 5;
var lastState = 10;

client.on('ready', () => {
    console.log('I am ready!');
    sendNotification("Hallo, ich bin der Monitor-Bot \"Uptime Robot\". Ich benachrichtige euch wenn der Server offline oder wieder verfügbar ist :upside_down_face:");
});


client.login(config.discordBotToken);

// Check Monitor by interval in seconds
setInterval(callUptimeRobot, checkInterval * 1000);

function checkMonitor(jsonResponse) {

    var utrResponse = JSON.parse(jsonResponse);
    
    var monitor = utrResponse.monitors[0];

    // synchronise interval with UTR
    checkInterval = monitor.interval;
    
    if (monitor.status === 9 && lastState != 9) {
        sendNotification("Der Server ist aktuell nicht erreichbar :sob: Betroffene Dienste: TeamSpeak3, Gameserver, FTP-Server, Arma3Sync und Datenbankserver...");
    }
    else {
        if (lastState >= 8 && monitor.status == 2) {
            sendNotification("Der Server ist nun erreichbar :muscle: Die Dienste werden in den nächsten paar Sekunden bis Minuten wieder zur Verfügung stehen.");
        }
    }

    lastState = monitor.status;
}

function sendNotification(msg) {
    client.channels.get(config.channelId).send(msg);
}

function callUptimeRobot() {
    var request = require("request");

    var options = {
        method: 'POST',
        url: 'https://api.uptimerobot.com/v2/getMonitors',
        headers:
        {
            'cache-control': 'no-cache',
            'content-type': 'application/x-www-form-urlencoded'
        },
        form: { api_key: config.monitorApiKey, format: 'json', logs: '1' }
    };

    request(options, function (error, response, body) {
        if (error) console.warn(error);

        checkMonitor(body);
    });

}



